'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
    'ngRoute',
    'ui.bootstrap',
    'angular-perfect-scrollbar-2',
    'modelService',
    'userTrackingService',
    'productFeedService',
    'ngAnimate',
    'angular-preload-image',
    'myApp.productFinder',
    'myApp.resultsView',
    'myApp.version',
    'rzModule',
    'chart.js'
]);
app.config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/productFinder', {
          templateUrl: 'controllers/productFinder/productFinder.html',
          controller: 'ProductFinderCtrl'
      }).
      when('/userData', {
          templateUrl: 'controllers/userData/userData.html',
          controller: 'userDataCtrl'
      }).
      otherwise({
          redirectTo: '/productFinder'
      });

}]);
app.run(['productFeedService', function(productFeedService) {
    productFeedService.init();
}]);
