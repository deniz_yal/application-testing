
angular.module('userTrackingService',[]).factory('userTrackingService', ['$cookies','$window',function($cookies,$window) {

    var user = {};
    var trackingStack = [];
    var previousEvent={};
    var cookieManager = function(){

        var cookieKey = "sb-userTracking";

        var putCookies = function(){

            var cookieObject = {
                "user":user,
                "events":trackingStack
            };

            $cookies.putObject(cookieKey,cookieObject);
        };

        var getCookies = function(){

            var cookieObject = $cookies.getObject(cookieKey);

            if(cookieObject && cookieObject["user"] && cookieObject["events"]){
                user = cookieObject["user"];
                trackingStack = cookieObject["events"];
            }
            removeCookies();
        };

        var removeCookies = function(){
            $cookies.remove(cookieKey);
        };

        return {
            putCookies: putCookies,
            getCookies: getCookies,
            removeCookies: removeCookies
        }
    }();

    /*user browser type display in short version*/
    function getBrowserShortName(userAgent) {

        /*Regex rule set*/
        var browsers = {chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i};

        /*Searching key words in long format of user browser data*/
        for(var key in browsers) {
            if (browsers[key].test(userAgent)) {
                return key;
            }
        }

        return 'unknown';
    }

    /*Generate random Id for user*/
    function generateUserId()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        /*Choosing random character in char list*/
        for( var i=0; i < 10; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    var init = function(){

        /*Get previous tracking event from cookie*/
       if(!user || !user.Id || trackingStack.length<1){
           cookieManager.getCookies();
        }

        /*When new user came generate default user variables*/
        if(!user.Id){
            user.Id = generateUserId();
            user.LoginDate = new Date();

            var navigator = $window.navigator;
            user.Browser = navigator.userAgent;
            user.BrowserShort = getBrowserShortName(navigator.userAgent);

            user.Language = navigator.language;
            user.Platform = navigator.platform;
        }

        /*First tracking event when app init*/
        track({
            name:"appLaunched"
        });
    };

    var track = function(data){

        var eventDate = new Date();
        var tracking = {};
        var timeInterval = 0;
        var from = "";

        /*Get previous event data and calculate tracking parameter*/
        if(Object.getOwnPropertyNames(previousEvent).length > 0){

            /*calculate spending time at event*/
            timeInterval=eventDate-previousEvent.date;
            /*get previous event name*/
            from=previousEvent.name;
        }

        /*extend data type for add default parameters to tracking event*/
        angular.extend(tracking, data, {date: eventDate , timeInterval:timeInterval, from:from});
        trackingStack.push(tracking);

        /*using for store previous event data*/
        previousEvent = tracking;

        /*update user last active date*/
        user.LastActiveDate=eventDate;

        /*store events at cookie*/
        cookieManager.putCookies();
    };

    var getCurrentUser = function(){
        return user;
    };

    var getTrackingResult = function(){
        return trackingStack;
    };

    init();

    return {
        init: init,
        track: track,
        getTrackingResult: getTrackingResult,
        getCurrentUser:getCurrentUser
    };
}]);
