/**
 * Created by jean on 5/23/2016.
 */

angular.module('productFeedService',[]).factory('productFeedService', function($http) {

    var csvData;

    var csvLoaded = false;

    var funcQueue = [];

    var csvRead = function(csvData){
        var rows = csvData.split("\n");
        var retVal = [];
        var separator = "\t";
        var columnsName = rows.shift();
        var regexpRemoveCtrlChars = /[\x00-\x1F\x7F-\x9F]/g;
        columnsName = columnsName.split(separator).map(function(v){ return v.replace(regexpRemoveCtrlChars, "")});
        rows.forEach(function(row){
            var elements = row.split(separator);
            var processedElement = {};
            elements.forEach(function(elt, index){
                processedElement[columnsName[index]] = elt.replace(regexpRemoveCtrlChars, "");
            });
            retVal[retVal.length] = processedElement;
        });
        return retVal;
    };

    var init = function(){
        //TODO FIXME As soon as kinderzimmerhaus fixes the Allow Access control origin all, put the url here instead of the local file
        $http.get("./data_test/kinderbetten-finder.csv").then( function(response){
                csvData = csvRead(response.data);
            console.log(csvData);

                csvLoaded = true;
                funcQueue.forEach(function(fn){
                    fn();
                });
            }
        )
    };

    //TODO FIXME This function is useless for the code, it is just to understand better the data stucture provided by Kinderzimmer
    var getPossibleValues = function(arguments, fnCallback){
        var possibleValues = {};
        arguments.forEach(function(elt){
            possibleValues[elt] = [];
            csvData.forEach(function(cd){
                if(possibleValues[elt].indexOf(cd[elt]) == -1){
                    possibleValues[elt].push(cd[elt]);
                }
            })
        });
        console.log(possibleValues);
    };


    var getProducts = function(dictArguments, fnCallback){
        if(csvLoaded){
            var filteredResults = csvData;
            // console.log(csvData);
                var count = 0;

            angular.forEach(dictArguments, function(val, key){
                if(val === undefined)
                    return true;
                var re = new RegExp(' ', 'g');
                val = val.toLowerCase().replace(re, '');
                
                console.log('----------');
                console.log(key, val);
                 
                count = 0;
                filteredResults = filteredResults.filter(function(d){
                    var csvValue = "";
                    if (d[key] != undefined){ csvValue = d[key].toLowerCase().replace(re, '');}
                    if(csvValue.indexOf(val) !== -1)
                        count++;
                    return csvValue.indexOf(val) !== -1; //csvValue == val || (csvValue.indexOf(",") != -1 && csvValue.split(/[ ]?,[ ]?/).indexOf(val) != -1);
                });
                console.log(count);
                fnCallback(filteredResults);

            });
        } else {
            funcQueue.push(this.getProducts.bind(this, dictArguments, fnCallback));
        }
    };

    init();

    return {
        init: init,
        getProducts: getProducts,
        getPossibleValues: getPossibleValues
    };
});
