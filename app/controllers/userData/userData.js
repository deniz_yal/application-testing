/**
 * Created by DenizRaklet on 8/16/2016.
 */

    'use strict';

    angular.module("myApp").controller("userDataCtrl",["$scope","userTrackingService",
        function ($scope,userTrackingService) {

            /*group event array by selected object variable
            * It s using in  chart data and label generating*/
            function groupBy( array , f )
            {
                var groups = {};
                array.forEach( function( o )
                {
                    var group = JSON.stringify( f(o) );
                    groups[group] = groups[group] || [];
                    groups[group].push( o );
                });
                return Object.keys(groups).map( function( group )
                {
                    return groups[group];
                })
            }

            /*Generate variables for display data in bar chart*/
            var getBarChart = function (eventList) {
                var barChart = {};
                var labels=[];
                var data=[];

                var result = groupBy(eventList, function(item)
                {
                    return [item.name];
                });

                result.forEach( function( i )
                {
                    if(i.length>0 && i[0]['name']){
                        labels.push(i[0]['name']);
                        data.push(i.length)
                    }
                });

                barChart.labels = labels;
                barChart.series = ['count'];
                barChart.data = data;
                barChart.isVisible = data.length > 0;

                return barChart;
            };

            /*Generate variables for display data in pie chart*/
            var getPieChart = function (eventList) {
                var pieChart = {};
                var labels=[];
                var data=[];

                var result = groupBy(eventList, function(item)
                {
                    return [item.name];
                });

                result.forEach( function( i )
                {
                    if(i.length>0 && i[0]['name'] && i[0]['timeInterval']){
                        labels.push(i[0]['name']);
                        var sum = 0;
                        i.forEach( function( j )
                        {
                            sum+=j['timeInterval'];
                        });
                        data.push(sum)
                    }
                });

                pieChart.labels = labels;
                pieChart.data = data;
                pieChart.isVisible = data.length > 0;

                return pieChart;
            };

            /*Generate variables for display data in line chart*/
            var getLineChart = function (eventList) {
                var lineChart = {};
                var labels=[];
                var data=[];
                var optionClickedEvents=[];
                eventList.forEach(function( i )
                {
                    if(i.hasOwnProperty('name')&& i.hasOwnProperty('attr')&& i['name']==="optionClicked"){
                        optionClickedEvents.push(i['attr']);
                    }
                });

                var result = groupBy(optionClickedEvents, function(item)
                {
                    return [item.option];
                });

                result.forEach( function( i )
                {
                    if(i.length>0 && i[0]['option']){
                        labels.push(i[0]['option']);
                        data.push(i.length)
                    }
                });

                lineChart.labels = labels;
                lineChart.series = ['count'];
                lineChart.data = data;
                lineChart.isVisible = data.length > 0;


                return lineChart;
            };

            /*Manage event listing at view*/
            var getTrackingEventList = function (data,orderProp,quantity) {
                var trackingEventList = {};
                trackingEventList.result = data;
                trackingEventList.orderProp = orderProp;
                trackingEventList.quantity = quantity;
                return trackingEventList;
            };

            /*user tracking data for graphs*/
            var trackData = userTrackingService.getTrackingResult();

            /*Scope variables*/
            $scope.user = userTrackingService.getCurrentUser();
            $scope.trackDataList = getTrackingEventList(trackData,'date',100);
            $scope.bar = getBarChart(trackData);
            $scope.pie = getPieChart(trackData);
            $scope.line = getLineChart(trackData);
    }]);
