/**
 * Created by DenizRaklet on 8/17/2016.
 */

angular.module('myApp').directive('userEventListItem', [function() {
    return {
        restrict: 'E',
        scope: {
            item: '<data'
        },
        templateUrl: 'components/userEventList/userEventListItem/userEventListItem.html'
    };
}]);