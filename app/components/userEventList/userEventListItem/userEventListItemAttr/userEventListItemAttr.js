/**
 * Created by DenizRaklet on 8/17/2016.
 */

angular.module('myApp').directive('userEventListItemAttr', [function() {
    return {
        restrict: 'E',
        scope: {
            attrVal: '<datavalue',
            attrKey: '<datakey'
        },
        templateUrl: 'components/userEventList/userEventListItem/userEventListItemAttr/userEventListItemAttr.html',
        controller: function ($scope) {
            var attrVal = $scope.attrVal;
            $scope.isObjectWirhAttr = Object.keys(attrVal).length>0 && typeof($scope.attrVal)==="object";
            $scope.isSingleAttr = attrVal && typeof($scope.attrVal)!=="object";
        }
    };
}]);