/**
 * Created by DenizRaklet on 8/17/2016.
 */

angular.module('myApp').directive('userEventList', [function() {
    return {
        restrict: 'E',
        scope: {
            events: '<data'
        },
        templateUrl: 'components/userEventList/userEventList.html'
    };
}]);